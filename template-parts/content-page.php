<?php

/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package tcoif
 */
$link = get_page_link();
?>

<article>
	<?php if ("http://localhost/wordpress/" != $link) : ?>
		<header class="entry-header">
			<?php the_title('<h1 class="entry-title">', '</h1>'); ?>
		</header><!-- .entry-header -->


	<?php endif; ?>

	<div class="entry-content">
		<?php
		the_content();

		wp_link_pages(array(
			'before' => '<div class="page-links">' . esc_html__('Pages:', 'tcoif'),
			'after'  => '</div>',
		));
		?>
	</div><!-- .entry-content -->

	<?php if (get_edit_post_link()) : ?>
		<footer class="entry-footer">
			<?php
			edit_post_link(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__('Edit <span class="screen-reader-text">%s</span>', 'tcoif'),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
			?>
		</footer><!-- .entry-footer -->
	<?php endif;
	if ("http://localhost/wordpress/" == $link) : ?>
		<h2>les 2 derniers articles</h2>
		<?php
		$arguments = array(
			// type de contenu = article
			'post_type' => 'post',
			// n'affiche que les articles qui sont publiés
			'post_status' => 'publish',
			// combien je veux afficher d'éléments par boucle
			'posts_per_page' => 2,
			// ordre par date
			'orderBy' => 'date',
			// ordre descendent (derniers articles)
			'order' => 'DESC',
		);

		$my_query = new WP_Query($arguments);

		if ($my_query->have_posts()) :
		?>
			<ul>
				<?php
				// documentation https://codex.wordpress.org/The_Loop#Nested_Loops
				while ($my_query->have_posts()) :
					$my_query->the_post();
				?>
					<li>
						<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					</li>
				<?php
				endwhile;
				?>
			</ul>
	<?php
		endif;
		wp_reset_postdata();
	endif;
	?>
</article>