<?php

/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package tcoif
 */

?>

<article class="liste-articles">
    <header class="entry-header-articles">
        <?php
        if (is_singular()) :
            the_title('<h1 class="entry-title-articles">', '</h1>');
        else :
            the_title('<h2 class="entry-title-articles"><a href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a></h2>');
        endif;
        ?>
    </header><!-- .entry-header -->

    <div class="entry-content-articles">
        <?php
        the_excerpt(sprintf(
            wp_kses(
                /* translators: %s: Name of current post. Only visible to screen readers */
                __('Continue reading<span class="screen-reader-text-articles"> "%s"</span>', 'tcoif'),
                array(
                    'span' => array(
                        'class' => array(),
                    ),
                )
            ),
            get_the_title()
        ));
        ?>
        <div class="entry-date-articles">
            <?php the_time('d/m/Y'); ?>
        </div><!-- .entry-date-articles -->
    </div><!-- .entry-content -->
</article>