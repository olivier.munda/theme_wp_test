<?php

/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package tcoif
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main">

		<section class="error-404 not-found">
			<header class="page-header-404">
				<h1 class="page-title"><?php esc_html_e('Oups! Cette page est introuvable.', 'tcoif'); ?></h1>
				<?php get_search_form(); ?>
			</header><!-- .page-header-404 -->
			<div class="page-content-404">
				<img id="img404" src="https://media.giphy.com/media/A9EcBzd6t8DZe/giphy.gif" alt="giphy">
				<div>
					<h2>Liste des pages</h2>
					<?php
					$arguments = array(
						// type de contenu = page
						'post_type' => 'page',
						// n'affiche que les pages qui sont publiés
						'post_status' => 'publish',
						// combien je veux afficher d'éléments par boucle
						'posts_per_page' => 2,
						// ordre par date
						'orderBy' => 'date',
						// ordre descendent (derniers pages)
						'order' => 'DESC',
					);

					$my_query = new WP_Query($arguments);

					if ($my_query->have_posts()) :
					?>
						<ul>
							<?php
							// documentation https://codex.wordpress.org/The_Loop#Nested_Loops
							while ($my_query->have_posts()) :
								$my_query->the_post();
							?>
								<li>
									<a href="<?php the_permalink();  ?>"><?php the_title(); ?></a>
								</li>
							<?php
							endwhile;
							?>
						</ul>
					<?php
					endif; ?>
					<h2>les 5 derniers articles</h2>
					<?php
					$arguments = array(
						// type de contenu = article
						'post_type' => 'post',
						// n'affiche que les articles qui sont publiés
						'post_status' => 'publish',
						// combien je veux afficher d'éléments par boucle
						'posts_per_page' => 5,
						// ordre par date
						'orderBy' => 'date',
						// ordre descendent (derniers articles)
						'order' => 'DESC',
					);

					$my_query = new WP_Query($arguments);

					if ($my_query->have_posts()) :
					?>
						<ul>
							<?php
							// documentation https://codex.wordpress.org/The_Loop#Nested_Loops
							while ($my_query->have_posts()) :
								$my_query->the_post();
							?>
								<li>
									<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
								</li>
							<?php
							endwhile;
							?>
						</ul>
					<?php
					endif;
					wp_reset_postdata();
					?>
				</div>
			</div><!-- .page-content-404 -->
		</section><!-- .error-404 -->

	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
