<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package tcoif
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<title><?php bloginfo('name'); ?> - <?php bloginfo('description'); ?></title>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php wp_body_open(); ?>
	<div id="page" class="site">
		<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e('Skip to content', 'tcoif'); ?></a>
		<?php if ("http://localhost/wordpress/404" == $link) : ?>
			<img width="1920" height="829" src="http://localhost/wordpress/wp-content/uploads/2020/03/water-3244961_1920.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="http://localhost/wordpress/wp-content/uploads/2020/03/water-3244961_1920.jpg 1920w, http://localhost/wordpress/wp-content/uploads/2020/03/water-3244961_1920-300x130.jpg 300w, http://localhost/wordpress/wp-content/uploads/2020/03/water-3244961_1920-1024x442.jpg 1024w, http://localhost/wordpress/wp-content/uploads/2020/03/water-3244961_1920-768x332.jpg 768w, http://localhost/wordpress/wp-content/uploads/2020/03/water-3244961_1920-1536x663.jpg 1536w" sizes="(max-width: 1920px) 100vw, 1920px">
		<?php else : tcoif_post_thumbnail();
		endif; ?>
		<header id="masthead" class="site-header">
			<div class="site-branding">
				<?php
				the_custom_logo();
				if (is_front_page() && is_home()) :
				?>
					<h1 class="site-title"><a href="<?php echo esc_url(home_url('/')); ?>" rel="home"><?php bloginfo('name'); ?></a></h1>
				<?php
				else :
				?>
					<p class="site-title"><a href="<?php echo esc_url(home_url('/')); ?>" rel="home"><?php bloginfo('name'); ?></a></p>
				<?php
				endif;
				$tcoif_description = get_bloginfo('description', 'display');
				if ($tcoif_description || is_customize_preview()) :
				?>
					<p class="site-description"><?php echo $tcoif_description; /* WPCS: xss ok. */ ?></p>
				<?php endif; ?>
			</div><!-- .site-branding -->

			<nav id="site-navigation" class="main-navigation">
				<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e('Primary Menu', 'tcoif'); ?></button>
				<?php
				wp_nav_menu(array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
				));
				?>
			</nav><!-- #site-navigation -->
		</header><!-- #masthead -->

		<div id="content" class="site-content">